# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: apivtora <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/16 12:08:17 by apivtora          #+#    #+#              #
#    Updated: 2017/03/16 12:30:23 by apivtora         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fractol
FLAG = -Wall -Werror -Wextra -lmlx -framework OpenGL -framework AppKit
SRC = ft_dragon.c ft_hooks.c ft_julia3.c ft_line.c ft_mandelbrot.c ft_piftree.c ft_spyder.c main.c ft_choose.c ft_gcol.c ft_julia.c ft_key.c ft_madel3.c ft_menu.c ft_pixel_put.c ft_str_create.c
OBJ = ft_dragon.o ft_hooks.o ft_julia3.o ft_line.o ft_mandelbrot.o ft_piftree.o ft_spyder.o main.o ft_choose.o ft_gcol.o ft_julia.o ft_key.o ft_madel3.o ft_menu.o ft_pixel_put.o ft_str_create.o

HEADER = fractol.h

all : $(NAME)

$(NAME) : $(OBJ)
	gcc -o $(NAME) $(OBJ) $(FLAG) 

%.o: %.c
	gcc -o $@ -c $< -Wall -Werror -Wextra 

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all
