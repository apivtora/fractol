/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/01 11:51:48 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/20 14:14:40 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <mlx.h>
# include <math.h>

typedef struct	s_fract
{
	int			x;
	int			y;
	int			i;
	float		c_im;
	float		c_re;
	float		z_im;
	float		z_re;
	float		tmp;
}				t_fract;
typedef struct	s_tree
{
	int			x0;
	int			y0;
	int			x1;
	int			y1;
	int			angle;
}				t_tree;
typedef struct	s_line
{
	int			x0;
	int			y0;
	int			x1;
	int			y1;
	int			dx;
	int			sx;
	int			dy;
	int			sy;
	int			err;
	int			e2;
}				t_line;
typedef struct	s_mlx_info
{
	void		*mlx;
	void		*win;
	void		*img;
	char		*col;
	int			bpp;
	int			size_line;
	int			endian;
	float		zoom;
	int			dep;
	int			cx;
	int			cy;
	int			shift_x;
	int			shift_y;
	int			mx;
	int			my;
	t_tree		*t;
	int			fractal;
	int			mouse;
	int			color;
	int			menu;
}				t_mlx_info;
t_mlx_info		*ft_str_create(void);
void			ft_pixel_put(t_mlx_info *fr, int x, int y, int color);
int				ft_gcol(int i, t_mlx_info *fr);
int				exp_hook_t(t_mlx_info *fr);
int				key(int keycode, t_mlx_info *fr);
int				mouse(int keycode, int x_m, int y_m, t_mlx_info *fr);
int				mouse_move(int x, int y, t_mlx_info *fr);
void			ft_mandelbrot(t_mlx_info *fr);
void			ft_julia(t_mlx_info *fr);
void			ft_mandel3(t_mlx_info *fr);
void			ft_julia3(t_mlx_info *fr);
void			ft_spider(t_mlx_info *fr);
void			ft_line(int *first, int *second, t_mlx_info *fr);
void			ft_piftree(t_mlx_info *fr);
void			ft_dragon(t_mlx_info *fr);
void			ft_choose(t_mlx_info *fr);
void			ft_menu(t_mlx_info *fr);
#endif
