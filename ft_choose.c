/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_choose.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/13 16:24:38 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/15 16:45:12 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_choose(t_mlx_info *fr)
{
	if (fr->fractal == 1)
		ft_mandelbrot(fr);
	else if (fr->fractal == 2)
		ft_julia(fr);
	else if (fr->fractal == 3)
		ft_mandel3(fr);
	else if (fr->fractal == 4)
		ft_julia3(fr);
	else if (fr->fractal == 5)
		ft_spider(fr);
	else if (fr->fractal == 6)
		ft_piftree(fr);
	else if (fr->fractal == 7)
		ft_dragon(fr);
}
