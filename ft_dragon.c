/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dragon.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/13 16:22:15 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/15 15:48:37 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_draw(t_tree t, t_tree t1, t_mlx_info *fr)
{
	int first[2];
	int second[2];

	first[0] = t1.x0;
	first[1] = t1.y0;
	second[0] = t1.x1;
	second[1] = t1.y1;
	ft_line(first, second, fr);
	first[0] = t1.x1;
	first[1] = t1.y1;
	second[0] = t.x1;
	second[1] = t.y1;
	ft_line(first, second, fr);
}

static void	ft_dragon_body(t_tree t, t_mlx_info *fr, int i)
{
	t_tree	t1;
	t_tree	t2;
	int		temp;

	t1.x0 = t.x0;
	temp = sqrt(pow((t.x0 - t.x1), 2) + pow((t.y0 - t.y1), 2)) / sqrt(2);
	t1.x1 = t1.x0 + roundf(temp * cos((t.angle + 45) * 3.14159 / 180));
	t1.angle = t.angle + 45;
	t1.y0 = t.y0;
	t1.y1 = t1.y0 + roundf(temp * sin((t.angle + 45) * 3.14159 / 180));
	if (i == fr->dep - 20)
		ft_draw(t, t1, fr);
	else
	{
		ft_dragon_body(t1, fr, i + 1);
		t2.x0 = t.x1;
		t2.x1 = t1.x1;
		t2.y0 = t.y1;
		t2.y1 = t1.y1;
		t2.angle = t.angle + 135;
		ft_dragon_body(t2, fr, i + 1);
	}
}

void		ft_dragon(t_mlx_info *fr)
{
	t_tree t;

	fr->dep = fr->dep < 35 ? fr->dep : 35;
	fr->dep = fr->dep > 20 ? fr->dep : 20;
	t.x0 = 250;
	t.x1 = 850;
	t.y0 = 400;
	t.y1 = 400;
	t.angle = 0;
	ft_dragon_body(t, fr, 0);
}
