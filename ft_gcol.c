/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gcol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/10 13:39:33 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/15 16:46:16 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	ft_gcol(int i, t_mlx_info *fr)
{
	int k_r;
	int k_g;
	int k_b;

	k_r = (fr->color >> 16) / fr->dep;
	k_g = ((fr->color >> 8) & 0xff) / fr->dep;
	k_b = (fr->color & 0xff) / fr->dep;
	return (((i * k_r) << 16) + ((i * k_g) << 8) + i * k_b);
}
