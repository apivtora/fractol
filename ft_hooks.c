/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hooks.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 12:50:34 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/16 12:43:53 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	exp_hook_t(t_mlx_info *fr)
{
	fr->img = mlx_new_image(fr->mlx, 1000, 1000);
	fr->col = mlx_get_data_addr(fr->img, &(fr->bpp),
			&(fr->size_line), &(fr->endian));
	ft_choose(fr);
	mlx_put_image_to_window(fr->mlx, fr->win, fr->img, 0, 0);
	ft_menu(fr);
	mlx_destroy_image(fr->mlx, fr->img);
	return (0);
}

int	mouse(int keycode, int x_m, int y_m, t_mlx_info *fr)
{

	if (keycode == 4)
	{
		fr->zoom *= 1.1;
		fr->shift_x = fr->shift_x - ((x_m - fr->cx) - fr->shift_x) * 0.1;
		fr->shift_y = fr->shift_y - ((y_m - fr->cy) - fr->shift_y) * 0.1;
	}
	else if (keycode == 5)
	{
		fr->zoom *= 0.9;
		fr->shift_x = fr->shift_x + ((x_m - fr->cx) - fr->shift_x) * 0.1;
		fr->shift_y = fr->shift_y + ((y_m - fr->cy) - fr->shift_y) * 0.1;
	}
	exp_hook_t(fr);
	return (0);
}

int	mouse_move(int x, int y, t_mlx_info *fr)
{
	if (x > 0 && x < 1000 && y > 0 && y < 1000 && fr->mouse < 0)
	{
		fr->mx = x;
		fr->my = y;
		exp_hook_t(fr);
	}
	return (0);
}
