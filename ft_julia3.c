/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_julia3.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 14:47:39 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/15 16:13:06 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_julia3_body(t_mlx_info *fr, t_fract t, float z_im, float z_re)
{
	t.z_re = ((float)t.x) / (300.0 * fr->zoom);
	t.z_im = ((float)t.y) / (300.0 * fr->zoom);
	t.c_im = z_im;
	t.c_re = z_re;
	t.i = 0;
	while (t.i < fr->dep)
	{
		t.tmp = t.z_re * t.z_re * t.z_re - 3 * t.z_im * t.z_im * t.z_re;
		t.z_im = -t.z_im * t.z_im * t.z_im + 3 * t.z_im * t.z_re *
			t.z_re + t.c_im;
		t.z_re = t.tmp + t.c_re;
		if (t.z_re * t.z_re + t.z_im * t.z_im > 4)
			break ;
		t.i++;
	}
	if (t.i < fr->dep)
		ft_pixel_put(fr, t.x + 500 + fr->shift_x,
				t.y + 500 + fr->shift_y, ft_gcol(t.i, fr));
	else
		ft_pixel_put(fr, t.x + 500 + fr->shift_x,
				t.y + 500 + fr->shift_y, 0x4B0082);
}

void		ft_julia3(t_mlx_info *fr)
{
	t_fract	t;
	float	z_im;
	float	z_re;

	z_im = 0.002 * (500 - fr->my);
	z_re = 0.001 * (500 - fr->mx);
	fr->cx = 500;
	fr->cy = 500;
	t.y = -500 - (fr->shift_y);
	while (t.y < 500 - (fr->shift_y))
	{
		t.x = -500 - (fr->shift_x);
		while (t.x < 500 - (fr->shift_x))
		{
			ft_julia3_body(fr, t, z_im, z_re);
			t.x++;
		}
		t.y++;
	}
}
