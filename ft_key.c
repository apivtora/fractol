/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_key.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/15 15:48:54 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/16 12:32:13 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_fract_reset(int keycode, t_mlx_info *fr)
{
	if (keycode == 18)
		fr->fractal = 1;
	else if (keycode == 19)
		fr->fractal = 2;
	else if (keycode == 20)
		fr->fractal = 3;
	else if (keycode == 21)
		fr->fractal = 4;
	else if (keycode == 23)
		fr->fractal = 5;
	else if (keycode == 22)
		fr->fractal = 6;
	else if (keycode == 26)
		fr->fractal = 7;
	if (keycode == 82)
	{
		fr->shift_x = 0;
		fr->shift_y = 0;
		fr->zoom = 1;
		fr->dep = 20;
		fr->mx = 500;
		fr->my = 500;
	}
}

static void	ft_ch_col(int keycode, t_mlx_info *fr)
{
	if (keycode == 83)
		fr->color = 0x1e90ff;
	else if (keycode == 84)
		fr->color = 0x8B008B;
	else if (keycode == 85)
		fr->color = 0xffffff;
	else if (keycode == 86)
		fr->color = 0xFFD700;
	else if (keycode == 87)
		fr->color = 0xFF4500;
	else if (keycode == 88)
		fr->color = 0x9ACD32;
	else if (keycode == 89)
		fr->color = rand() / 255;
}

int			key(int keycode, t_mlx_info *fr)
{
	if (keycode == 53)
		exit(0);
	ft_fract_reset(keycode, fr);
	ft_ch_col(keycode, fr);
	if (keycode == 69)
		fr->dep++;
	else if (keycode == 78)
		fr->dep--;
	if (keycode == 123)
		fr->shift_x += 5;
	else if (keycode == 124)
		fr->shift_x -= 5;
	else if (keycode == 126)
		fr->shift_y += 5;
	else if (keycode == 125)
		fr->shift_y -= 5;
	if (keycode == 76)
		fr->mouse = -fr->mouse;
	if (keycode == 67)
		fr->menu *= -1;
	exp_hook_t(fr);
	return (0);
}
