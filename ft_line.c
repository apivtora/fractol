/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_line.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 16:52:54 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/15 16:16:33 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_line(int *first, int *second, t_mlx_info *fr)
{
	t_line l;

	l.dx = abs(second[0] - first[0]);
	l.sx = first[0] < second[0] ? 1 : -1;
	l.dy = abs(second[1] - first[1]);
	l.sy = first[1] < second[1] ? 1 : -1;
	l.err = (l.dx > l.dy ? l.dx : -(l.dy)) / 2;
	while (1)
	{
		ft_pixel_put(fr, first[0], first[1], 0xFFFFFF);
		if (first[0] == second[0] && first[1] == second[1])
			break ;
		l.e2 = l.err;
		if (l.e2 > -(l.dx))
		{
			l.err = l.err - l.dy;
			first[0] += l.sx;
		}
		if (l.e2 < l.dy)
		{
			l.err += l.dx;
			first[1] += l.sy;
		}
	}
}
