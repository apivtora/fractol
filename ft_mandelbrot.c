/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mandelbrot.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/15 16:26:10 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/15 16:35:00 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_mandelbrot_body(t_mlx_info *fr, t_fract t,
		float z_im, float z_re)
{
	t.c_re = ((float)t.x) / (300.0 * fr->zoom);
	t.z_im = z_im;
	t.z_re = z_re;
	t.i = 0;
	while (t.i < fr->dep)
	{
		t.tmp = t.z_re * t.z_re - t.z_im * t.z_im;
		t.z_im = 2 * t.z_re * t.z_im + t.c_im;
		t.z_re = t.tmp + t.c_re;
		if (t.z_re * t.z_re + t.z_im * t.z_im > 4)
			break ;
		t.i++;
	}
	if (t.i < fr->dep)
		ft_pixel_put(fr, t.x + 600 + fr->shift_x,
				t.y + 500 + fr->shift_y, ft_gcol(t.i, fr));
	else
		ft_pixel_put(fr, t.x + 600 + fr->shift_x,
				t.y + 500 + fr->shift_y, 0x4B0082);
}

void		ft_mandelbrot(t_mlx_info *fr)
{
	t_fract	t;
	float	z_im;
	float	z_re;

	z_im = 0.002 * (500 - fr->my);
	z_re = 0.001 * (500 - fr->mx);
	fr->cx = 600;
	fr->cy = 500;
	t.y = -500 - (fr->shift_y);
	while (t.y < 500 - (fr->shift_y))
	{
		t.x = -600 - (fr->shift_x);
		t.c_im = ((float)t.y) / (300.0 * fr->zoom);
		while (t.x < 400 - (fr->shift_x))
		{
			ft_mandelbrot_body(fr, t, z_im, z_re);
			t.x++;
		}
		t.y++;
	}
}
