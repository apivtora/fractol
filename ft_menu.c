/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_menu.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/15 12:04:36 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/16 11:11:39 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#define MLX mlx_string_put

static void	ft_menu2(t_mlx_info *fr)
{
	MLX(fr->mlx, fr->win, 350, 40, 0x00FF8C00, "Transform");
	MLX(fr->mlx, fr->win, 300, 55, 0x00FF8C00,
				"Move up/down - Arrow up/down");
	MLX(fr->mlx, fr->win, 300, 70, 0x00FF8C00,
				"Move left/right - Arrow left/right");
	MLX(fr->mlx, fr->win, 300, 85, 0x00FF8C00,
				"Zoom in/out - Mouse wheel");
	MLX(fr->mlx, fr->win, 300, 100, 0x00FF8C00,
				"Increase/decrease iter. - NUM + / NUM -");
	MLX(fr->mlx, fr->win, 300, 115, 0x00FF8C00,
				"Change color - NUM 1 - NUM 7");
	MLX(fr->mlx, fr->win, 300, 130, 0x00FF8C00,
				"Mouse ON / OFF - NUM Enter");
}

void		ft_menu(t_mlx_info *fr)
{
	if (fr->menu > 0)
	{
		MLX(fr->mlx, fr->win, 400, 20, 0x00FF8C00, "Controls");
		MLX(fr->mlx, fr->win, 50, 40, 0x00FF8C00, "Change fractal:");
		MLX(fr->mlx, fr->win, 30, 55, 0x00FF8C00, "1 - Mandelbroth");
		MLX(fr->mlx, fr->win, 30, 70, 0x00FF8C00, "2 - Julia");
		MLX(fr->mlx, fr->win, 30, 85, 0x00FF8C00, "3 - Mandelbroth cubic");
		MLX(fr->mlx, fr->win, 30, 100, 0x00FF8C00, "4 - Julia cubic");
		MLX(fr->mlx, fr->win, 30, 115, 0x00FF8C00, "5 - Spider");
		MLX(fr->mlx, fr->win, 30, 130, 0x00FF8C00, "6 - Pithaghor's tree");
		MLX(fr->mlx, fr->win, 30, 145, 0x00FF8C00, "7 - Dragon");
		ft_menu2(fr);
		MLX(fr->mlx, fr->win, 750, 40, 0x00FF8C00, "Others");
		MLX(fr->mlx, fr->win, 700, 55, 0x00FF8C00, "Reset - NUM 0");
		MLX(fr->mlx, fr->win, 700, 70, 0x00FF8C00,
				"Hide controls NUM *");
		MLX(fr->mlx, fr->win, 700, 85, 0x00FF8C00, "Exit - ESC");
	}
}
