/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mlx_create.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 13:31:38 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/20 14:19:44 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_mlx *ft_mlx_create(void)
{
	t_mlx *rt;

	rt = (t_mlx*)malloc(sizeof(*rt));
	rt->mlx = NULL;
	rt->win = NULL;
	rt->img = NULL;
	rt->col = NULL;
	rt->bpp = 0;
	rt->size_line = 0;
	rt->endian = 0;
	return (rt);
}
