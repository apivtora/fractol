/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_piftree.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/12 15:51:01 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/15 16:37:50 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_piftree_body(t_tree t, t_mlx_info *fr, int i)
{
	int		temp;
	t_tree	t1;
	int		first[2];
	int		second[2];

	first[0] = t.x0 + 500;
	first[1] = -t.y0 + 999;
	second[0] = t.x1 + 500;
	second[1] = -t.y1 + 999;
	ft_line(first, second, fr);
	temp = sqrt(pow((t.x0 - t.x1), 2) + pow((t.y0 - t.y1), 2)) / sqrt(2);
	t1.x0 = t.x1;
	t1.x1 = t.x1 + temp * sin((t.angle + fr->my * 0.06) * 3.14 / 180);
	t1.y0 = t.y1;
	t1.y1 = t.y1 + temp * cos((t.angle + fr->my * 0.06) * 3.14 / 180);
	t1.angle = t.angle + fr->my * 0.06;
	if (i < fr->dep - 18)
	{
		ft_piftree_body(t1, fr, i + 1);
		t1.x1 = t.x1 + temp * sin((t.angle - fr->my * 0.06) * 3.14 / 180);
		t1.y1 = t.y1 + temp * cos((t.angle - fr->my * 0.06) * 3.14 / 180);
		t1.angle = t.angle - fr->my * 0.06;
		ft_piftree_body(t1, fr, i + 1);
	}
}

void		ft_piftree(t_mlx_info *fr)
{
	t_tree t;

	t.x0 = 0;
	t.y0 = 0;
	t.x1 = 0;
	fr->zoom = fr->zoom < pow(1.1, 5) ? fr->zoom : pow(1.1, 5);
	fr->dep = fr->dep < 30 ? fr->dep : 30;
	fr->dep = fr->dep > 20 ? fr->dep : 20;
	t.y1 = 200 * fr->zoom;
	t.angle = 0;
	ft_piftree_body(t, fr, 0);
}
