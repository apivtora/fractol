/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pixel_put.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 11:32:58 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/16 11:12:26 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_pixel_put(t_mlx_info *fr, int x, int y, int color)
{
	fr->col[y * fr->size_line + 4 * x] = color & 0xff;
	fr->col[y * fr->size_line + 4 * x + 1] = (color >> 8) & 0xff;
	fr->col[y * fr->size_line + 4 * x + 2] = (color >> 16) & 0xff;
}
