/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_create.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 11:10:17 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/16 11:19:37 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

t_mlx_info	*ft_str_create(void)
{
	t_mlx_info *fr;

	fr = (t_mlx_info*)malloc(sizeof(*fr));
	fr->mlx = NULL;
	fr->win = NULL;
	fr->img = NULL;
	fr->col = NULL;
	fr->bpp = 0;
	fr->size_line = 0;
	fr->endian = 0;
	fr->dep = 20;
	fr->zoom = 1;
	fr->cx = 500;
	fr->cy = 500;
	fr->shift_x = 0;
	fr->shift_y = 0;
	fr->mx = 500;
	fr->my = 500;
	fr->mouse = 1;
	fr->color = 0x1e90ff;
	fr->menu = 1;
	return (fr);
}
