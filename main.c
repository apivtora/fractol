/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/01 12:08:10 by apivtora          #+#    #+#             */
/*   Updated: 2017/03/16 11:14:14 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	main(int argc, char **argv)
{
	t_mlx_info *fr;

	if (argc != 2 || (argv[1][0] < '1' || argv[1][0] > '7'))
	{
		write(1, "ERROR: wrong number of arguments (should be one)\n", 49);
		write(1, "Use numbers 1 - 7 to open fractals\n", 35);
		write(1, "1 - Mandelbroth\n2 - Julia\n3 - Mandelbroth cubic\n", 48);
		write(1, "4 - Julia cubic\n5 - Spyder\n6 - Pithaghor's tree\n", 48);
		write(1, "7 - Dragon\n", 11);
		exit(0);
	}
	fr = ft_str_create();
	fr->fractal = argv[1][0] - '0';
	fr->mlx = mlx_init();
	fr->win = mlx_new_window(fr->mlx, 1000, 1000, "Fract'ol");
	mlx_expose_hook(fr->win, exp_hook_t, fr);
	mlx_hook(fr->win, 2, 5, key, fr);
	mlx_hook(fr->win, 6, 0, mouse_move, fr);
	mlx_mouse_hook(fr->win, mouse, fr);
	mlx_loop(fr->mlx);
}
