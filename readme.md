# Fract'ol #
This is UNIT factory (school 42) studiying graphical project that can draw  
seven different fractals (five agebraic and two geometric). I takes only number  
of fractal you want to see as argument. You can change displayed fractal by keys  
during runtime (1-8 keys). Starting conditions can be changed just by moving  
mouse across the window. You can change fractals calor, iteration number etc. by num  
keys. Main feature is that you can zoom in as long as calculation persition  
allows.  
Full controls are displayed on output window.

## Some examples ##  

![exmaple 1](https://bytebucket.org/apivtora/fractol/raw/a2983d99a0094721bfa482d0e8ff2fd22e3ef21f/Screenshots/Screenshot%20from%202017-11-20%2019-45-23.png?token=48659ce47797e96fe428d7f550010d1d7817b492)
![exmaple 2](https://bytebucket.org/apivtora/fractol/raw/a2983d99a0094721bfa482d0e8ff2fd22e3ef21f/Screenshots/Screenshot%20from%202017-11-20%2019-46-06.png?token=ec6fdd1ead96d36ebfe02684813868955a7cf9e9)
![exmaple 3](https://bytebucket.org/apivtora/fractol/raw/a2983d99a0094721bfa482d0e8ff2fd22e3ef21f/Screenshots/Screenshot%20from%202017-11-20%2019-46-37.png?token=fecb52306c0ee52c60ff09bfb2ea0a96562e231f)
![exmaple 4](https://bytebucket.org/apivtora/fractol/raw/a2983d99a0094721bfa482d0e8ff2fd22e3ef21f/Screenshots/Screenshot%20from%202017-11-20%2019-47-02.png?token=a205d481ae2e0049b0237acbbc3a94dc27b5bd74)
![exmaple 5](https://bytebucket.org/apivtora/fractol/raw/a2983d99a0094721bfa482d0e8ff2fd22e3ef21f/Screenshots/Screenshot%20from%202017-11-20%2019-49-56.png?token=3a0092df434eae8ad70f0b539f7aedf5dce66ddc)
![exmaple 6](https://bytebucket.org/apivtora/fractol/raw/a2983d99a0094721bfa482d0e8ff2fd22e3ef21f/Screenshots/Screenshot%20from%202017-11-20%2019-49-29.png?token=d28205a47fac9d44b926e3c3ca705437804e25ed)

### Project details ###

This project is written for macOS(OS X El Capitan)  
using MinilibX graphic library (https://github.com/abouvier/minilibx)  
and it is needed to run this project  
Use 'make' to install project  

If you are running Linux you need to add -lm flag to compiling flags, as well as flags for MinilibX  
specified in its documentation. Also keycodes for Linux differ from macOS, so controls may differ  

### Disclaimer ###

This project is written according to 42's "The Norm" which specifies special rules for code like  
less then 25 lines in each function, 85 symbols in line forbidden most of libraries and functions that wasnt  
written by you, also "for", "switch case", "goto" are forbidden etc...   
You can read more at (https://ncoden.fr/datas/42/norm.pdf)  
So to achieve some conditions sometimes code is modified to the point where it bacomes not really readble  

